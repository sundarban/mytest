<?php

/**
 * This class is used for working on coupon.
 *
 * @author Sundar Ban sundarban@lftechnology.com
 */
class Coupon extends CController
{

	/**
	 * This function is used validate the coupon
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param array	$coupons contain the coupon code
	 * @return boolean True if True
	 */
	public static function IsvalidCoupon($coupons)
	{
		foreach ($coupons as $value) {
			$check = Coupon::checkCouponAvailability($value);
			if (!$check) {
				return FALSE;
			}
		}

		return TRUE;
	}

	/**
	 * This function is to see available coupon
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param array	$code contain the coupon code
	 * @return boolean True if True
	 */
	public static function checkCouponAvailability($code)
	{
		$criteria = new CDbCriteria;
		$criteria->mergeWith(array(
				'select'		 => '1',
				'condition'	 => 'CURDATE() between startDate and endDate and isDeleted=0 and status="Active" and couponCode="' . $code . '"'
		));

		$couponData = Coupons::model()->findAll($criteria);

		if (empty($couponData)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	/**
	 * This function is used to check coupon limit
	 *
	 * @author  Sundar Ban <sundarban@lftechnology.com>
	 * @param array	$codes contain the coupon code
	 * @return boolean True if True
	 */
	public static function IsCouponLimit($codes)
	{

		foreach ($codes as $code) {
			$sql = "SELECT couponLimit FROM coupons where couponCode='" . $code . "'";
			$couponLimit = Common::getSqlResult($sql);
			$couponLimitCount = $couponLimit[0]['couponLimit'];

			if ($couponLimitCount == 0) {
				return FALSE;
			}
		}

		return TRUE;
	}

	/**
	 * This function is used to check multiple coupon use of same vendor
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param array	$codes
	 * @return boolean True if True
	 */
	public static function IsMultipleCouponofVendor($codes)
	{

		$vendorsId = array();

		foreach ($codes as $code) {

			$sql = "SELECT vendorId FROM coupons where couponCode='" . $code . "'";
			$vendorIdByCoupon = Common::getSqlResult($sql);
			$vendorId = $vendorIdByCoupon[0]['vendorId'];

			if (in_array($vendorId, $vendorsId)) {
				return FALSE;
			}
			$vendorsId[] = $vendorId;
		}
		return TRUE;
	}

	/**
	 * This function is used to check authorized coupons of respective vendors
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param array	$codes int $userId
	 * @return boolean True if True
	 */
	public static function IsAuthorizedCoupon($userId, $codes)
	{
		$vendorsId = $vendorsIdByCouponCode = array();

		foreach ($codes as $code) {

			$sql = "SELECT vendorId FROM coupons WHERE couponCode='" . $code . "'";
			$vendorIdByCoupon = Common::getSqlResult($sql);
			if (!empty($vendorIdByCoupon)) {
				$vendorId = $vendorIdByCoupon[0]['vendorId'];
				$vendorsIdByCouponCode[] = $vendorId;
			}
		}

		$sql = "SELECT DISTINCT cd.vendorId FROM cart c
						INNER JOIN cartDetails cd ON c.id = cd.cartId
						WHERE c.customerId =$userId";

		$vendorIdByUserId = Common::getSqlResult($sql);

		foreach ($vendorIdByUserId as $i => $v) {
			$vendorsId[] = $v['vendorId'];
		}

		for ($i = 0; $i < count($vendorsIdByCouponCode); $i++) {
			if (!in_array($vendorsIdByCouponCode[$i], $vendorsId)) {

				return FALSE;
			}
		}

		return TRUE;
	}

	/**
	 * This function is used to check item added to cart
	 * @param  int $userId
	 * @return boolean True if True
	 */
	public static function IsCheckCart($userId)
	{
		$cartProductByUser = Cart::model()->findByAttributes(array(
				'customerId' => $userId
		));

		if (empty($cartProductByUser)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	/**
	 * This function is used to check item added to cart
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param  array $coupons
	 * @return boolean True if True
	 */
	public static function reduceCouponCount($coupons)
	{

		foreach ($coupons as $codes) {
			$sql = "UPDATE coupons
							SET couponLimit = couponLimit - 1
							WHERE couponCode ='$codes' ";

			$cmd = Yii::app()->db->createCommand($sql);
			$coupon = $cmd->execute();

			if (!$coupon) {
				return FALSE;
			}
		}
		return FALSE;
	}

	/**
	 * This function is used to check item added to cart
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param  array $codes
	 * @param int $userId
	 * @return boolean True if True
	 */
	public static function IsCouponLimitPerUser($codes, $userId)
	{

		$CouponTimesUsedByUser = 0;
		$couponLimitPerUser = 0;

		foreach ($codes as $code) {
			$sql = "SELECT COUNT(distinct orderId) AS totalUses FROM orders ord
							INNER JOIN orderVendor ordven ON ord.id=ordven.orderId
							WHERE ordven.couponcode= '" . $code . "  ' AND ord.customerId=$userId  ";

			$CouponTimesUsed = Common::getSqlResult($sql);
			$CouponTimesUsedByUser = $CouponTimesUsed[0]['totalUses'];

			$sql = "SELECT userLimit FROM coupons where couponCode='" . $code . "'";
			$couponLimit = Common::getSqlResult($sql);
			$couponLimitPerUser = $couponLimit[0]['userLimit'];


			if ($CouponTimesUsedByUser >= $couponLimitPerUser) {
				return FALSE;
			}
		}
		return TRUE;
	}

	/**
	 * This function is used to check item added to cart
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param  int $userId
	 * @param float $amount
	 * @return boolean True if True
	 */
	public static function IsAvailableCreditAmount($userId, $amount)
	{

		$userData = Users::model()->findByPk((int) $userId);
		$avilableCredit = $userData->referrals;

		if ($amount <= $avilableCredit) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * This function is used to check total uses of coupons
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param string $couponCode
	 * @return int $CouponTimesUsedByUser
	 */
	public static function TotalCouponUsed($couponCode)
	{
		$CouponTimesUsedByUser = 0;

		$sql = "SELECT COUNT(distinct orderId) AS totalUses FROM orders ord
							INNER JOIN orderVendor ordven ON ord.id=ordven.orderId
							WHERE ordven.couponcode= '" . $couponCode . "  '";

		$CouponTimesUsed = Common::getSqlResult($sql);
		$CouponTimesUsedByUser = $CouponTimesUsed[0]['totalUses'];

		return $CouponTimesUsedByUser;
	}

	/**
	 * This function is used to get vendorId from coupons
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param array $coupons
	 * @return array $vendorsId
	 */
	public static function getVendorIdFromCoupon($coupons)
	{
		$vendorsId = array();

		foreach ($coupons as $code) {

			$couponInfo = Coupons::model()->findByAttributes(array(
					'couponCode' => $code
			));

			$vendorsId[$couponInfo->vendorId] = $couponInfo->couponCode;
		}
		return $vendorsId;
	}

	/**
	 * This function is used to map respective coupon to vendor
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param array $totalAmount
	 * @param array $coupons
	 * @return array $couponMapVendor
	 */
	public static function MappingVendorAndCoupon($totalAmount, $coupons)
	{
		$couponMapVendor = $amount = array();
		$i = 0;

		foreach ($totalAmount as $keyt => $valt) {
			foreach ($coupons as $keyc => $valc) {
				if ($keyt == $keyc) {
					$couponMapVendor[$valc] = $valt . 'v' . $keyt;
				}
			}
		}

		foreach ($totalAmount as $a => $b) {
			if (!in_array($b . 'v' . $a, $couponMapVendor)) {
				$couponMapVendor['NULLCOUPON' . $i++] = $b;
			}
		}

		foreach ($couponMapVendor as $key => $val) {
			if (substr($key, 0, 10) == "NULLCOUPON") {
				$amount[$key] = $val;
			} else {
				$amount [$key] = (float) Coupon::getAmount($val);
			}
		}

		return $amount;
	}

	/**
	 * This function is used to get couponcode use in specic order
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param int $orderId
	 * @param array $couponData
	 */
	public static function getCouponData($orderId)
	{
		$couponData = $vendorAmount = array();

		$couponInfoSql = "SELECT DISTINCT couponCode from orderVendor WHERE orderId=" . $orderId;
		$couponValue = Common::getSqlResult($couponInfoSql);

		for ($i = 0; $i < count($couponValue); $i++) {
			if (!empty($couponValue[$i]['couponCode'])) {

				$vendor = $couponInfo = OrderVendor::model()->findByAttributes(array(
								'couponCode' => $couponValue[$i]['couponCode'],
								'orderId'		 => $orderId
						))->attributes;

				$couponData[(int) $vendor['vendorId']] = $couponValue[$i]['couponCode'];
			}
		}
		return $couponData;
	}

	public static function getAmount($val)
	{
		$amountArray = array();
		if ($val) {
			$amountArray = explode("v", $val);
			return $amountArray[0];
		}
	}

}