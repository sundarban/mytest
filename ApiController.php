<?php

/**
 * ApiController is extended from CController <br/>
 * Added basic functions for api-controllers
 *
 * @author Sundar Ban <sundarban@lftechnology.com>
 */
class ApiController extends CController
{

	/**
	 * Check app level of security.
	 *
	 * @param boolean $showError define wheather display authentication error message or not.
	 * @return boolean true if authorized else return false.
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 */
	public static function authenticateApp()
	{
		$msg = '';

		$headers = ApiFunctions::getAllHeaders();
		if (isset($headers['Privatekey'])) {
			$authenticatePameters = array('privateKey' => $headers['Privatekey']);
			$authenticate = ApiFunctions::authenticateApp($authenticatePameters);

			if ($authenticate) {
				return TRUE;
			} else {
				$msg = 'Incorrect Private Key.';
				ApiController::errorPrivateKey($msg);
				Yii::app()->end();
			}
		} else {
			$msg = 'Missing private key parameter in header';
			ApiController::errorPrivateKey($msg);
			Yii::app()->end();
		}
	}

	/**
	 * Check app level of security if Private key is not set
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param string $msg
	 * @return array $data
	 *
	 */
	public static function errorPrivateKey($msg)
	{
		$data = array();

		$statusHeader = 'HTTP/1.1 ' . Constants::$statusCode . ' ' . Constants::$statusText;
		header($statusHeader);
		header('Content-type: application/json');

		$data['msg'] = $msg;
		$data['responseCode'] = Message::$error;
		echo CJSON::encode($data);
		Yii::app()->end();
	}

	/**
	 * Check if a valid token for user
	 *
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 * @param int $userId
	 * @param string $userToken
	 * @return boolean if TRUE
	 *
	 */
	public static function authenticateUser($userToken)
	{

		$token = UserWebToken::model()->findByAttributes(array(
				'token' => $userToken
		));

		if ($token) {
			return TRUE;
		} else {
			$msg = 'Invalid User Token.';
			ApiController::errorPrivateKey($msg);
			Yii::app()->end();
		}
	}

}