<?php

/**
 * This class is used for calculation of the user order.
 *
 * @author Sundar Ban sundarban@lftechnology.com
 */
class Calculation extends CController
{

    /**
     * This function is used to get totalAmount of products accroding to vendor
     * @param int $userId array $coupons
     * @return array $result
     */
    public static function getTotalAmountofProductsByVendor($userId, $coupons)
    {
        $sums = $eachVendor = $result = array();
        $total = 0;

        $sql = "SELECT cd.vendorId,cd.quantity,cd.productOptionId
						FROM cart c
						INNER JOIN cartDetails cd ON c.id = cd.cartId
						WHERE c.customerId =$userId";

        $cartResult = Common::getSqlResult($sql);

        for ($i = 0; $i < count($cartResult); $i++) {
            $sums[] = Calculation:: GetTotalSum($cartResult[$i]);
        }

        foreach ($sums as $sum) {
            foreach ($sum as $vendorId => $amount) {
                $eachVendor[$vendorId][] = $amount;
                if (isset($result[$vendorId])) {
                    $result[$vendorId] += $amount;
                } else {
                    $result[$vendorId] = $amount;
                }
            }
        }

        if (empty($coupons)) {
            foreach ($result as $vendorId => $sum) {
                $total+=$sum;
            }
            return $total;
        } else {
            return $result;
        }
    }

    /**
     * This function is used to get sum of products
     * @param  array $data
     * @return array $total
     */
    public static function GetTotalSum($data)
    {

        $quantity = $data['quantity'];

        $sql = "SELECT price as Amount FROM productoption WHERE id=" . $data['productOptionId'];
        $result = Common::getSqlResult($sql);
        $totalAmount = $result[0]['Amount'] * $quantity;



        return $total = array($data['vendorId'] => $totalAmount);
    }

    /**
     * This function is used to add tax to vendor amount
     * @param  array $data
     * @return array $total
     */
    public static function getVendorAmountwithTax($shippingId, $total)
    {

        $sql = "SELECT st.taxPercent as taxPercent FROM customerAddress ca
						INNER JOIN state st ON ca.stateId=st.id WHERE ca.id= $shippingId";

        $userAddress = Common::getSqlResult($sql);

        if (empty($userAddress)) {
            return FALSE;
        } elseif ($total == 'tax') {

            return $userAddress[0]['taxPercent'];
        } else {
            $amountWithTax = $total + ($total * ($userAddress[0]['taxPercent'] / 100));
        }
        return $amountWithTax;
    }

    /**
     * This function is used to get coupon discount
     * @param  array $couponPerVendor
     * @return array $totalAmount
     */
    public static function getCouponDiscountByVendor($couponPerVendor)
    {
        $totalAmount = 0;

        foreach ($couponPerVendor as $key => $value) {

            if (substr($key, 0, 10) == "NULLCOUPON") {
                $totalAmount+=$value;
            } else {

                $couponInfo = Coupons::model()->findByAttributes(array(
                            'couponCode' => $key
                        ))->attributes;


                if ($value >= $couponInfo['minimumPurchase']) {
                    switch ($couponInfo['discountType']) {

                        case "Percentage":

                            $totalAmount += $value - ($value * (double) $couponInfo['discountAmount'] / 100);

                            break;

                        case "Currency":
                            $totalAmount +=$value - (double) $couponInfo['discountAmount'];
                            break;

                        default :
                            return FALSE;
                    }
                } else {
                    $totalAmount+=$value;
                }
            }
        }

        return $totalAmount;
    }

    /**
     * This function is used to get shipping charge for order product
     * @param  int $customerId
     * @return int $shippingCharge
     */
    public static function getShippingCharge($customerId)
    {
        $shippingCharge = 0;

        $sql = "SELECT * FROM cart c
						INNER JOIN cartDetails cd ON c.id = cd.cartId
						WHERE c.customerId =$customerId";
        $result = Common::getSqlResult($sql);

        for ($i = 0; $i < count($result); $i ++) {
            $vendorId = $result[$i]['vendorId'];

            $CustomerCountryId = CustomerAddress::model()->findByAttributes(array('customerId' => $customerId))->attributes['countryId'];
            $vendorCountryId = Vendors::model()->findByAttributes(array('userId' => $vendorId))->attributes['countryId'];

            if ($CustomerCountryId == $vendorCountryId) {

                $rate = 'p.shippingRate as Rate';
            } else {

                $rate = 'p.internationalShippingRate as Rate';
            }

            $sql = "SELECT $rate FROM products p INNER JOIN productoption po ON p.id=po.productId where po.id=" . $result [$i]['productOptionId'];
            $productInfo = Common::getSqlResult($sql);

            $shippingCharge +=$result[$i]['quantity'] * $productInfo [0]['Rate'];
        }

        return $shippingCharge;
    }

    /**
     * This function is used update transactionId of payment
     * @param  int $orderId string $transactionid
     * @return boolean IF TRUE
     */
    public static function updateOrderTransactionId($orderId, $transactionId)
    {

        if ($orderId && $transactionId) {
            $user = Orders::model()->findByPk((int) $orderId);
            $user->transactionId = (string) $transactionId;
            $user->update();
        }

        return TRUE;
    }

}