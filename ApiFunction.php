<?php

/**
 * ApiFunctions class<br/>
 * Content basic functions for api
 *
 * @author Sundar Ban <sundarban@lftechnology.com>
 */
Class ApiFunctions
{

	/**
	 * This function response data in json format
	 *
	 * @param array $data define data which have to response in json format.
	 * @param number $statusCode define http status header code
	 * @param string $statusText define http status header text
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 */
	public static function response($data, $statusCode, $statusText = NULL)
	{
		$headers = ApiController::authenticateApp();

		if ($headers == TRUE) {
			if ($statusText == NULL) {
				$statusText = ApiFunctions::getStatusCodeMessage($statusText);
			}

			$statusHeader = 'HTTP/1.1 ' . $statusCode . ' ' . $statusText;
			header($statusHeader);
			header('Content-type: application/json');
			$responseData = $data;
			echo json_encode($responseData);
			Yii::app()->end();
		}
	}

	/**
	 * This function return default http status header text for specific status code
	 *
	 * @param number $statusCode define status code
	 * @return string status code text
	 */
	public static function getStatusCodeMessage($statusCode)
	{
		$codes = Array(
				200	 => 'OK',
				204	 => 'No Content',
				400	 => 'Bad Request',
				401	 => 'Unauthorized',
				404	 => 'Not Found',
				500	 => 'Internal Server Error',
		);

		return (isset($codes[$statusCode])) ? $codes[$statusCode] : '';
	}

	/**
	 * Authenticate App
	 *
	 * @param array $authenticatePameters content parameters for authenticate
	 * @return boolean true if authorized else return false.
	 * @todo Currently app authentication is done in static way. This will be changed later.
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 */
	public static function authenticateApp($authenticatePameters)
	{
		if ($authenticatePameters['privateKey'] == 'QUAA4GNADCBiQKBgQCqGKukO') {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Get header data
	 *
	 * @return array headers
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 */
	public static function getAllHeaders()
	{
		$headers = '';
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
	}

	/**
	 * Generate ramdom string
	 *
	 * @param int $length length of string have to generate
	 * @return string random string
	 * @author Sundar Ban <sundarban@lftechnology.com>
	 */
	public static function randomString($length)
	{
		$possibleCharacters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$code = '';
		$i = 0;

		while ($i < $length) {
			$code .= substr($possibleCharacters, mt_rand(0, strlen($possibleCharacters) - 1), 1);
			$i++;
		}

		return $code;
	}

}