<?php

/**
 * OrdersController
 *
 * This controller is responsibe for Order API.
 * This controller enables to see their order details.
 *
 * @author Sundar Ban <sundarban@lftechnology.com>
 *
 */
class OrdersController extends ApiController
{

    /**
     * This controller is used to update the last login date.
     *
     * @author sundarban <sundarban@lftechnology.com>
     * @param object $action
     * @return boolean true if success
     */
    public function beforeAction($action)
    {
        ApiController::authenticateApp();

        $userId = Yii::app()->request->getParam('id');

        $userToken = Yii::app()->request->getParam('userToken');
        ApiController::authenticateUser($userToken);

        if (Common::updateLastLogin($userId)) {
            return TRUE;
        }

        parent::beforeAction($action);
    }

    /**
     * This function is used list the orders of customers
     * @param $id userId
     * @return array $ordersListing
     *
     *  @author sundarban <sundarban@lftechnology.com>
     */
    public function actionListOrders()
    {

        try {
            $userId = Yii::app()->request->getParam('id');
            $statusCode = 200;
            $statusText = '';
            $data['GET'] = $_GET;
            $data['POST'] = $_POST;

            $isUserExist = Common::isUserExists($userId); //checks if user exists or not
            if ($isUserExist) {
                $isCustomer = Orders::checkCustomer($userId);
                if ($isCustomer) {

                    $totalPerOrder = Order::getOrderById($userId, $orderList = 'list');
                    $data['orders'] = $totalPerOrder;

                    $data['responseCode'] = "SUCCESS";
                    $data['msg'] = Message::$ordersListing;
                } else {
                    $data['responseCode'] = "ERROR";
                    $data['msg'] = Message::$noPaymentCustomer;
                }
            } else {
                $data['responseCode'] = "ERROR";
                $data['msg'] = Message::$noUser;
            }
            ApiFunctions::response($data, $statusCode, $statusText);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    /**
     * This function is used to get orderdetail by orderId
     * @param int $userId $orderId
     * @return array $ordersListing
     *
     *  @author sundarban <sundarban@lftechnology.com>
     */
    public function actionOrderDetail()
    {
        $data = array();

        try {
            $userId = Yii::app()->request->getParam('id');
            $orderId = Yii::app()->request->getParam('orderId');

            $isUserExist = Common::isUserExists($userId); //checks if user exists or not

            if ($isUserExist) {

                $isCustomer = Order::checkCustomerOrder($userId, $orderId);
                if ($isCustomer) {
                    $orderDetails = Order::getOrderDetailById($userId, $orderId);

                    $orderInfo = Order::getSingleOrderAttribute($userId, $orderId);

                    $data['orders'] = $orderDetails;
                    $data['orderDetail'] = $orderInfo;



                    $data['responseCode'] = "SUCCESS";
                    $data['msg'] = Message::$orderDetails;
                } else {
                    $data['responseCode'] = "ERROR";
                    $data['msg'] = Message::$noPaymentCustomer;
                }
            } else {
                $data['responseCode'] = "ERROR";
                $data['msg'] = Message::$noUser;
            }

            $statusCode = 200;
            $statusText = '';
            $data['GET'] = $_GET;
            $data['POST'] = $_POST;

            ApiFunctions::response($data, $statusCode, $statusText);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

}