
<?php

/**
 * Vendor Controller
 *
 * This controller is responsibe Vendor .
 * This controller is used for vendor information.
 *
 * @author sundarban <sundarban@lftechnology.com>
 *
 */
Class VendorsController extends ApiController
{

	/**
	 * This controller is used to update the last login date.
	 *
	 * @author sundarban <sundarban@lftechnology.com>
	 * @param object $action
	 * @return boolean true if success
	 */
	public function beforeAction($action)
	{
		ApiController::authenticateApp();

		$userId = Yii::app()->request->getParam('id');

		$userToken = Yii::app()->request->getParam('userToken');
		ApiController::authenticateUser($userToken);

		if (Common::updateLastLogin($userId)) {
			return TRUE;
		}

		parent::beforeAction($action);
	}

	/**
	 * This function is used to list all the vendor information.
	 * @param int $id.
	 * @return boolean if TRUE
	 *
	 * @author sundarban <sundarban@lftechnology.com>
	 */
	public function actionListVendors()
	{

		$id = Yii::app()->request->getParam('id');
		if (empty($id)) {
			$statusCode = 500;
			$statusText = '';
			$data['responseCode'] = "ERROR";
			$data['msg'] = 'Empty Data.';
		} else {
			$vendorInfo = Vendor::getVendorInformation($id);
			if (empty($vendorInfo)) {
				$statusCode = 200;
				$statusText = '';
				$data['responseCode'] = "ERROR";
				$data['msg'] = 'No data listed';
				$data['GET'] = $_GET;
				$data['POST'] = $_POST;
			} else {
				$statusCode = 200;
				$statusText = '';
				$data['responseCode'] = "SUCCESS";
				$data['msg'] = 'Vendor list';
				$data['GET'] = $_GET;
				$data['POST'] = $_POST;
				$data['vendors'] = $vendorInfo;
			}
		}
		ApiFunctions::response($data, $statusCode, $statusText);
	}

	/**
	 * This function is used to get the vendor details.
	 * @return array $data
	 *
	 * @author sundarban <sundarban@lftechnology.com>
	 */
	public function actionVendorDetail()
	{
		$userId = Yii::app()->request->getParam('id');
		$vid = Yii::app()->request->getParam('vid');



		$vendorInfo = Users::model()->findByAttributes(array(
				'id'			 => $vid,
				'userType' => 'Vendor'
		));
		$userInfo = Common::isUserExists($userId);

		if (empty($vendorInfo)) {

			$data['responseCode'] = Message::$error;
			$data['msg'] = 'Vendor not found.';
		} elseif (empty($userInfo)) {

			$data['responseCode'] = Message::$error;
			$data['msg'] = 'User Not found.';
		} else {
			/*
			 * Save data when user views vendor profile
			 */

			$VendorView = Vendor::saveVendorView($userId, $vid);

			if ($VendorView) {

				$data['responseCode'] = Message::$success;
				$data['msg'] = 'Vendor Information.';
				$data['vendorData'] = Vendor::getVendorData($userId, $vid);
			} else {

				$data['responseCode'] = Message::$error;
				$data['msg'] = 'Vendor Profile view data not updated.';
			}


			$data['GET'] = $_GET;
			$data['POST'] = $_POST;
		}
		ApiFunctions::response($data, Constants::$statusCode, Constants::$statusText);
	}

	/**
	 * This function is used to get the vendor products.
	 * @return array $data
	 *
	 * @author sundarban <sundarban@lftechnology.com>
	 */
	public function actionVendorProducts()
	{
		$userId = Yii::app()->request->getParam('id');
		$vid = Yii::app()->request->getParam('vid');
		$pageNum = Yii::app()->request->getParam('page');
		$pageNum = isset($pageNum) ? $pageNum : 1;


		$colorsSearch = Yii::app()->request->getParam('colors') ? Yii::app()->request->getParam('colors') : '';
		$sizesSearch = Yii::app()->request->getParam('sizes') ? Yii::app()->request->getParam('sizes') : '';
		$fabricsSearch = Yii::app()->request->getParam('fabrics') ? Yii::app()->request->getParam('fabrics') : '';
		$categoriesSearch = Yii::app()->request->getParam('categories') ? Yii::app()->request->getParam('categories') : '';
		$vendorsSearch = Yii::app()->request->getParam('vendors') ? Yii::app()->request->getParam('vendors') : '';
		$maxprice = Yii::app()->request->getParam('maxprice') ? Yii::app()->request->getParam('maxprice') : '';
		$minprice = Yii::app()->request->getParam('minprice') ? Yii::app()->request->getParam('minprice') : '';

		$filterData = array(
				'colors'		 => $colorsSearch,
				'sizes'			 => $sizesSearch,
				'fabrics'		 => $fabricsSearch,
				'categories' => $categoriesSearch,
				'vendors'		 => $vendorsSearch,
				'maxprice'	 => $maxprice,
				'minprice'	 => $minprice
		);

		if (empty($userId)) {
			$statusCode = 500;
			$statusText = '';
			$data['responseCode'] = "ERROR";
			$data['msg'] = 'Empty Data.';
		} else {
			$vendorInfo = Users::model()->findByAttributes(array('id' => $vid, 'userType' => 'Vendor'));
			$userInfo = Users::model()->findByAttributes(array('id' => $userId, 'userType' => 'User'));

			if (empty($vendorInfo)) {
				$data['responseCode'] = "ERROR";
				$data['msg'] = 'Vendor not found.';
			} elseif (empty($userInfo)) {
				$data['responseCode'] = "ERROR";
				$data['msg'] = 'User Not found.';
			} else {

				$productList = Vendor::getAllProductsofVendor($userId, $vid, $pageNum, $filterData);

				$data['responseCode'] = "SUCCESS";
				$data['msg'] = 'Vendor Product Listing.';
				$data['products'] = $productList->products;
				$data['total_pages'] = $productList->total_pages;
				$data['current_page'] = $productList->current_page;
				$data['total_item_count'] = $productList->total_item_count;
			}
			$statusText = '';
			$statusCode = 200;
			$data['GET'] = $_GET;
			$data['POST'] = $_POST;
		}
		ApiFunctions::response($data, $statusCode, $statusText);
	}

	/**
	 * This function is used to get the vendor follwers.
	 * @return array $data
	 *
	 * @author sundarban <sundarban@lftechnology.com>
	 */
	public function actionVendorFollowers()
	{
		$userId = Yii::app()->request->getParam('id');
		$vid = Yii::app()->request->getParam('vid');
		$pageNum = Yii::app()->request->getParam('page');
		$pageNum = isset($pageNum) ? $pageNum : 1;

		if (empty($userId)) {
			$statusCode = 500;
			$statusText = '';
			$data['responseCode'] = "ERROR";
			$data['msg'] = 'Empty Data.';
		} else {
			$vendorInfo = Users::model()->findByAttributes(array(
					'id'			 => $vid,
					'userType' => 'Vendor'
			));
			$userInfo = Users::model()->findByAttributes(array(
					'id'			 => $userId,
					'userType' => 'User'
			));

			if (empty($vendorInfo)) {
				$data['responseCode'] = "ERROR";
				$data['msg'] = 'Vendor not found.';
			} elseif (empty($userInfo)) {
				$data['responseCode'] = "ERROR";
				$data['msg'] = 'User Not found.';
			} else {

				$userList = Vendor::getVendorFollowers($userId, $vid, $pageNum);

				$data['responseCode'] = "SUCCESS";
				$data['msg'] = 'Vendor Followers Listing.';
				$data['users'] = $userList->users;
				$data['total_pages'] = $userList->total_pages;
				$data['current_page'] = $userList->current_page;
				$data['total_item_count'] = $userList->total_item_count;
			}
			$statusText = '';
			$statusCode = 200;
			$data['GET'] = $_GET;
			$data['POST'] = $_POST;
		}
		ApiFunctions::response($data, $statusCode, $statusText);
	}

}