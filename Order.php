<?php

/**
 * This class is used for working on users order in api.
 *
 * @author Sundar Ban sundarban@lftechnology.com
 */
class Order extends CController
{

    /**
     * This function is used to fetch orders of the customer
     * @param $id userId
     * @return array $ordersListing
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function getOrderById($userId, $orderlist)
    {
        $total = 0;

        $connection = Yii::app()->db;
        if ($orderlist == 'list') {
            $query = $connection->createCommand()
                    ->select('id,createdDate,totalAmount')
                    ->from('orders')
                    ->where('customerId=:customerId', array(':customerId' => $userId))
                    ->order('createdDate desc');
        } else {
            $query = $connection->createCommand()
                    ->select('id,createdDate,totalAmount')
                    ->from('orders')
                    ->andWhere('customerId=:customerId', array(':customerId' => $userId))
                    ->andWhere("id=:id", array(':id' => $orderlist));
        }

        $result = $query->queryAll();

        for ($i = 0; $i < count($result); $i++) {
            $orderId = $result[$i]['id'];

            $status = Order::checkOrderStatus($orderId);
            $deliverStatus = ($status == TRUE) ? "Completed" : "Processing";

            $result[$i]['status'] = $deliverStatus;
            $result[$i]['totalAmountPerVendor'] = Order::getTotalAmount($orderId);
            $result[$i]['totalwithDiscount'] = Order::CouponDiscount($result[$i]['totalAmountPerVendor'], $orderId);
            $result[$i]['totalWithTax'] = Order::CalculateTax($result[$i]['totalwithDiscount'][0], $orderId);
            $result[$i]['totalShipping'] = Order::AddShippingCharge($result[$i]['totalWithTax'][0], $orderId);
            $result[$i]['total'] = "" . number_format((float) (( $result[$i]['totalShipping'][0] * 100)) / 100, 2);
            $result[$i]['image'] = Order::getPrimaryImage($orderId);

            if ($orderlist == "list") {
                unset($result[$i]['totalwithDiscount']);
                unset($result[$i]['totalAmountPerVendor']);
                unset($result[$i]['totalWithTax']);
                unset($result[$i]['totalShipping']);
            }
        }

        return $result;
    }

    /**
     * This function is used to get orderStatus
     * @param orderId $orderId
     * @return boolean If TRUE
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function checkOrderStatus($orderId)
    {
        $incompleteStatus = array("Processing", "Shipped", "Cancelled");

        $orderStatus = OrderVendor::model()->findAll(
                array("condition" => "orderId =  $orderId"));

        foreach ($orderStatus as $value) {
            if (in_array($value->orderStatus, $incompleteStatus)) {
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * This function is used to get total amount per vendor
     * @param int $orderId
     * @return array $resultTotal
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function getTotalAmount($orderId)
    {

        $sums = $resultTotal = $totalAmountByVendor = $eachVendor = array();

        $result = Order::getOrderDetailByOrderId($orderId);


        if (!empty($result) && is_array($result)) {
            for ($i = 0; $i < count($result); $i++) {
                $sums[] = Order::getTotalAmountperProduct($result[$i]);
            }

            foreach ($sums as $sum) {
                foreach ($sum as $vendorId => $amount) {
                    $eachVendor[$vendorId][] = $amount;
                    if (isset($resultTotal[$vendorId])) {
                        $resultTotal[$vendorId] += $amount;
                    } else {
                        $resultTotal[$vendorId] = $amount;
                    }
                }
            }
        }
        return $resultTotal;
    }

    /**
     * This function is used to get total amount per orderDetail
     * @param array $productRow
     * @return array $total
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function getTotalAmountperProduct($productRow)
    {
        $total = array();
        $prodOptId = $productRow['productOptionId'];
        $sql = "SELECT p.vendorId as vendorId
						FROM productoption pr
						INNER JOIN products p ON p.id = pr.productId
						WHERE pr.id =$prodOptId";

        $vendorInfo = Common::getSqlResult($sql);

        if (!empty($vendorInfo) && is_array($vendorInfo)) {
            $vendorId = $vendorInfo[0]['vendorId'];
            $totalAmount = $productRow['quantity'] * $productRow ['productPrice'];
            $total = array($vendorId => $totalAmount);
        }
        return $total;

        //old code
        /* $vendorId = $vendorInfo[0]['vendorId'];

          $totalAmount = $productRow['quantity'] * $productRow ['productPrice'];

          return $total = array($vendorId => $totalAmount); */
    }

    /**
     * This function is used to get coupon discount per vendor
     * @param int $orderId array $totalperVendor
     * @return int $totalAmount
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function CouponDiscount($totalPerVendor, $orderId)
    {

        $discount = $totalAmount = 0;
        $couponPerVendor = $couponData = $vendorAmount = array();

        $couponData = Coupon::getCouponData($orderId);

        if (empty($couponData)) {
            foreach ($totalPerVendor as $key => $value) {
                $totalAmount+=$value;
            }
        } else {
            $vendorAmount = Order::setAmountPerVendor($totalPerVendor);

            $couponPerVendor = Coupon::MappingVendorAndCoupon($vendorAmount, $couponData);

            foreach ($couponPerVendor as $key => $value) {

                if (substr($key, 0, 10) == "NULLCOUPON") {
                    $totalAmount+=$value;
                    $discount+= 0;
                } else {

                    $couponInfo = OrderVendor::model()->findByAttributes(array(
                                'couponCode' => $key,
                                'orderId' => $orderId
                            ))->attributes;

                    switch ($couponInfo['couponDiscountType']) {

                        case "Percentage":

                            $discount+=($value * (double) $couponInfo['couponDiscount'] / 100 );

                            $totalAmount += $value - ($value * (double) $couponInfo['couponDiscount'] / 100 );

                            break;

                        case "Currency":
                            $discount+=(double) $couponInfo['couponDiscount'];

                            $totalAmount +=$value - (double) $couponInfo['couponDiscount'];
                            break;

                        default :
                            return FALSE;
                    }
                }
            }
        }

        return $data = array(
            0 => $totalAmount,
            1 => number_format((float) $discount, 2, '.', ''));
    }

    /**
     * This function is used to calculate tax
     * @param int $totalDiscount $orderId
     * @return int $totalAmount
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function CalculateTax($totalWithDiscount, $orderId)
    {
        $taxPercent = Yii::app()->db->createCommand(array(
                    'select' => 'taxPercent',
                    'from' => 'orders',
                    'where' => 'id=' . $orderId
                ))->queryAll();

        $tax = $totalWithDiscount * ($taxPercent [0]['taxPercent'] / 100 );
        $totalAmount = $totalWithDiscount + $tax;


        return $data = array(0 => $totalAmount, 1 => $tax);
    }

    /**
     * This function is used to check Order of Customer
     * @param int $userId $orderId
     * @return boolean TRUE if true
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function checkCustomerOrder($userId, $orderId)
    {
        $checkOrder = Orders::model()->findByAttributes(array('customerId' => $userId, 'id' => $orderId));
        if (empty($checkOrder)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * This function is used add shipping charge
     * @param int $totalWithTax $orderId
     * @return int $ordersListing
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function AddShippingCharge($totalWithTax, $orderId)
    {
        $totalWithShippingCharge = 0;
        $result = Order::getOrderDetailByOrderId($orderId);

        for ($i = 0; $i < count($result); $i++) {
            $totalWithShippingCharge +=$result[$i]['quantity'] * $result [$i]['shippingPrice'];
        }

        $shppingCharge = $totalWithShippingCharge;

        $totalWithShippingCharge+=$totalWithTax;

        return $data = array(0 => $totalWithShippingCharge, 1 => $shppingCharge);
    }

    /**
     * This function is used to get orderDetailId by orderId
     * @param int $orderId
     * @return array $result
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function getOrderDetailByOrderId($orderId)
    {
        $orderVendorId = "";

        $ordersVendor = OrderVendor::model()->findAll(
                array("condition" => "orderId =  $orderId"));

        foreach ($ordersVendor as $orderVendor) {
            $orderVendorId .= "  $orderVendor->id  , ";
        }
        $orderVendorId .= "NULL";

        $sql = "SELECT * FROM orderDetails WHERE orderVendorId IN (" . $orderVendorId . ")";
        $result = Common::getSqlResult($sql);

        return $result;
    }

    /**
     * This function is used to get primaryImage for order
     * @param int $orderId
     * @return array $value
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function getPrimaryImage($orderId)
    {
        $value = array();

        $result = Order::getOrderDetailByOrderId($orderId);

        if (isset($result[0]['productOptionId'])) {

            $prodOptId = $result[0]['productOptionId'];

            $sql = "SELECT CONCAT('" . Yii::app()->params['imagePath'] . "','',pi.image) AS image
							FROM products p
							INNER JOIN productoption po ON p.id = po.productId
							INNER JOIN productimages pi ON p.id = pi.productId
							WHERE po.id =$prodOptId ";

            $value = Common::getSqlResult($sql);

            if (empty($value)) {
                return '';
            } else {
                return $value[0]['image'];
            }
        }
    }

    /**
     * This function is used to get order Detail
     * @param int $orderId $userId
     * @return array $orderDetail
     */
    public static function getOrderDetailById($userId, $orderId)
    {
        $sql = "SELECT ord.id as orderId,ord.totalAmount as totalAmount,od.productOptionId,od.quantity as orderedQty,ov.vendorId,ord.createdDate,od.productPrice as price,ov.orderStatus as status
						FROM orders ord
						INNER JOIN orderVendor ov ON ord.id = ov.orderId
						INNER JOIN orderDetails od ON ov.id = od.orderVendorId
						WHERE ord.customerId =$userId and ord.id=$orderId";

        $orderDetail = Common::getSqlResult($sql);

        for ($i = 0; $i < count($orderDetail); $i++) {

            $productId = Product::getProductIdByProductOptionId($orderDetail[$i]['productOptionId']);
            $productOptionAttribute = Productoption::model()->findByPk($orderDetail[$i]['productOptionId']);

            $orderDetail[$i]['productOptionCode'] = $productOptionAttribute->productOptionCode;
            $orderDetail[$i]['qty'] = $productOptionAttribute->qty;

            $productColor = $productOptionAttribute->color;
            $orderDetail[$i]['colorId'] = $productOptionAttribute->colorId;
            $orderDetail[$i]['colorName'] = $productColor['name'];

            $productSize = $productOptionAttribute->size;
            $orderDetail[$i]['sizeId'] = $productOptionAttribute->sizeId;
            $orderDetail[$i]['sizeName'] = $productSize['name'];

//			$productFabric = $productOptionAttribute->fabric;
//			$orderDetail[$i]['fabricId'] = $productOptionAttribute->fabricId;
//			$orderDetail[$i]['fabricname'] = $productFabric['name'];

            $orderDetail[$i]['fabricId'] = Product::getFabricIdByProductId($productId);
            $orderDetail[$i]['fabricname'] = Product::getFabricNameById($orderDetail[$i]['fabricId']);



            $orderDetail[$i]['product'] = Product::getProductDetailByPid($userId, $productId, $comment = FALSE);
        }

        return $orderDetail;
    }

    /**
     * This function is use to get order attribute
     * @param int $userId $orderId
     * @return array $orderAttributes
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function getSingleOrderAttribute($userId, $orderId)
    {
        $orderAttributes = array();
        $orderDetail = Orders::model()->findByPk($orderId);
        $tax = $orderDetail->taxPercent;
        $shippingAddressId = $orderDetail->shippingAddressId;
        $total = Order::getOrderById($userId, $orderId);

        $orderAttributes['orderId'] = $orderId;
        $orderAttributes['tax'] = $tax;
        $orderAttributes['taxAmount'] = $total[0]['totalWithTax'][1];
        $orderAttributes['discount'] = $total[0]['totalwithDiscount'][1];
        $orderAttributes['shipping'] = $total[0]['totalShipping'][1];
        $orderAttributes['total'] = (( number_format((float) $total[0]['total'], 2, '.', '') * 100)) / 100;
        $orderAttributes['ShippingAddress'] = CustomerAddress::model()->findByPk($shippingAddressId)->attributes;

        return $orderAttributes;
    }

    /**
     * This function is use to get customerId by OrderId
     * @param int $orderId
     * @return int array $userId
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function getUserIdByOrderId($orderId)
    {
        $userId = Orders::model()->findByAttributes(array('id' => $orderId));
        return $userId->attributes['customerId'];
    }

    /**
     * This function is send Email to vendor If new order is placed
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     * @param int $orderNumber
     * @return boolean If TRUE
     *
     */
    public static function sendNewOrderEmailToVendor($orderNumber)
    {

        $verifiedUser = '';

        $data = OrderVendor::model()->findAll('orderId=:orderId', array(
            ':orderId' => $orderNumber
        ));

        $vendorIds = array_map(function($details) {
            return $details['vendorId'];
        }, $data);

        foreach ($vendorIds as $vendorId) {

            Vendor::SaveNotificationStatus($vendorId);

            $checkEmailStatus = Notification::model()->findByAttributes(array(
                'vendorId' => $vendorId
            ));

            if (1 === (int) $checkEmailStatus->newOrder) {

                $vendorData = (array) Users::model()->findByAttributes(array(
                            'id' => $vendorId
                        ))->attributes;

                $vendorData['orderNumber'] = $orderNumber;

                $sendMail = Common::sendEmail($vendorData, $verifiedUser = "NewOrder");

                if (!$sendMail) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    /**
     * This function is used to send email to customer if vendor cancels order
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     * @param int $orderVendorId
     * @return boolean
     */
    public static function sendCancelOrderEmailToCustomer($orderVendorId)
    {
        $verifiedUser = '';

        $orderVendor = OrderVendor::model()->findByPk((int) $orderVendorId);

        $order = Orders::model()->findByPk($orderVendor->orderId);

        $orderDetail = OrderDetails::model()->findByAttributes(array(
            'orderVendorId' => (int) $orderVendorId
        ));
        $prodOptId = $orderDetail->productOptionId;

        $productId = Product::getProductIdByProductOptionId($prodOptId);

        $data = (array) Products::model()->findByAttributes(array(
                    'id' => (int) $productId
                ))->attributes;

        $data['userId'] = $order->customerId;
        $data['email'] = Users::model()->findByPk((int) $order->customerId)->email;
        $data['orderId'] = $orderVendor->orderId;

        $sendOrderCancelEmail = Common::sendEmail($data, $verifiedUser = "cancelOrder");

        if ($sendOrderCancelEmail) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    /**
     * This function is used to send email to vendor if product quantity is zero.
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     * @param int $orderId
     * @return boolean
     */
    public static function sendProductQtyNullMailtoVendor($orderId)
    {

        $data = OrderVendor::model()->findAll('orderId=:orderId', array(
            ':orderId' => $orderId
        ));

        $orderDetails = array_map(function($details) {
            return $details['id'];
        }, $data);

        foreach ($orderDetails as $orderDetailId) {

            $productAttributes = OrderDetails::model()->findByAttributes(array(
                'orderVendorId' => $orderDetailId
            ));

            $vendorId = OrderVendor::model()->findByAttributes(array(
                        'id' => $orderDetailId
                    ))->attributes['vendorId'];

            Vendor::SaveNotificationStatus($vendorId);

            $productNullNotification = Notification::model()->findByAttributes(array(
                'vendorId' => $vendorId
            ));

            if (1 === (int) $productNullNotification->itemSold) {

                $productQty = Productoption::model()->findByAttributes(array(
                    'id' => $productAttributes->productOptionId
                ));

                $productDetails = Products::model()->findByAttributes(array(
                    'id' => (int) $productQty->productId
                ));

                $vendorEmail = Users::model()->findByPk((int) $vendorId)->email;

                $data = array(
                    'productOpt' => $productQty,
                    'productDetails' => $productDetails,
                    'vendorEmail' => $vendorEmail
                );

                if ($data['productOpt']->qty == 0) {
                    Common::sendEmail($data, $verifiedUser = 'soldOut');
                }
            }
            return TRUE;
        }
    }

    /**
     * This function is used to send email to vendor if product stock is below 10 Percentage.
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     * @param int $orderId
     * @return boolean
     */
    public static function sendProductDropDownMailtoVendor($orderId)
    {
        $data = OrderVendor::model()->findAll('orderId=:orderId', array(
            ':orderId' => $orderId
        ));

        $orderDetails = array_map(function($details) {
            return $details['id'];
        }, $data);

        foreach ($orderDetails as $orderDetailId) {

            $totalQty = $availableQty = $totalQtySold = 0;

            $orderDetailsAttributes = OrderDetails::model()->findByAttributes(array(
                'orderVendorId' => $orderDetailId
            ));

            $vendorId = OrderVendor::model()->findByAttributes(array(
                        'id' => $orderDetailId
                    ))->attributes['vendorId'];

            Vendor::SaveNotificationStatus($vendorId);

            $productNullNotification = Notification::model()->findByAttributes(array(
                'vendorId' => $vendorId
            ));

            if (1 === (int) $productNullNotification->productDrop) {

                $ProductOtpionDetails = Productoption::model()->findByAttributes(array(
                    'id' => (int) $orderDetailsAttributes->productOptionId
                ));
                $availableQty = $ProductOtpionDetails->qty;

                $productDetails = Products::model()->findByAttributes(array(
                    'id' => (int) $ProductOtpionDetails->productId
                ));

                /* 		$productSoldDetail = OrderDetails::model()->findAllByAttributes(array(
                  'productOptionId' => (int) $orderDetailsAttributes->productOptionId
                  ));

                  $totalQtySold = array_sum(array_map(function($details) {
                  return $details['quantity'];
                  }, $productSoldDetail));

                  $totalQty = $availableQty + $totalQtySold;

                  $checkProductAvailabePercentage = ($availableQty / 100) * $totalQty; */

                $vendorEmail = Users::model()->findByPk((int) $vendorId)->email;

                $data = array(
                    'productOpt' => $ProductOtpionDetails,
                    'productDetails' => $productDetails,
                    'vendorEmail' => $vendorEmail
                );

                if ($availableQty < 10) {

                    Common::sendEmail($data, $verifiedUser = "productDropBelowTenPercen");
                }
            }
            return TRUE;
        }
    }

    /**
     * This function is used to map vendorID and Amount
     *
     * @author Sundar Ban <sundarban@lftechnology.com>
     * @param array $amountPerVendor
     * @return boolean
     */
    public static function setAmountPerVendor($amountPerVendor)
    {
        $amount = array();
        foreach ($amountPerVendor as $key => $value) {
            $index = str_replace("vendor", '', (string) $key);
            $amount[(int) $index] = $value;
        }
        return $amount;
    }

}