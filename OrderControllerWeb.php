<?php

/**
 * OrdersController
 *
 * This controller is responsibe for Order.
 * Vendor/Admin is able see the respective order from customer.
 *
 * @author Sundar Shrestah <sundarban@lftechnology.com>
 *
 */
class OrdersController extends Controller
{
    /*
     * set order layout for OrdersController.
     */

    public $layout = '//layouts/vendor';

    /**
     * Lists order of vendor products.
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionOrderList()
    {

        if (!isset(Yii::app()->user->userType)) {
            $this->redirect(Yii::app()->request->baseUrl . '/login');
        } else {
            $newState = Yii::app()->user->userType;
            Yii::app()->user->setState('userType', 'Vendor');
            $actualState = Yii::app()->user->getState('userType');
            if ($newState !== $actualState) {

                $this->redirect(Yii::app()->request->baseUrl . '/login');
            }
        }

        if (isset($_GET['key']) && !empty($_GET['key'])) {
            $searchKey = mysql_escape_string($_GET['key']);
            if (isset($searchKey) && !empty($searchKey)) {
                $query = Orders::getSearchQuery($searchKey); //sql raw query with search parameter
            }
        } else {
            $query = Orders::getQuery(); //sql raw query
        }
        //get order status
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        } else {
            $status = 'all';
        }
        //append on sql query based on status i.e all, pending, completed, cancelled
        $query.= Orders::getOrderStatusSql($status);

        //order sorting by ascending and descending order
        if (isset($_GET['sort']) && $_GET['sort'] == 'asc') {
            $orderBy = $_GET['sort']; //asc
            if ($orderBy == 'asc') {
                if (isset($_GET['sortKey']) && !empty($_GET['sortKey'])) {
                    $sortBy = $_GET['sortKey'];
                    $query.= Orders::getOrderSortAscSql($sortBy, $orderBy);
                }
                $orderClass = 'sorting-up';
            }
        } else if (isset($_GET['sort']) && $_GET['sort'] == 'desc') {
            $orderBy = $_GET['sort']; //desc
            if (isset($_GET['sortKey']) && !empty($_GET['sortKey'])) {
                $sortBy = $_GET['sortKey'];
                $query.= Orders::getOrderSortDescSql($sortBy, $orderBy);
            }
            $orderClass = 'sorting-down';
        } else {
            $orderClass = 'sorting';
            $query.= " ORDER BY o.id DESC";
        }

        $orderLists = Common::getSqlResult($query); //lists order
        if (is_array($orderLists) && !empty($orderLists)) {
            $uniqueOrderIds = OrdersController::getUniqueOrderId($orderLists); //list unique orderId
            $uniqueOrders = OrdersController::uniqueOrder($uniqueOrderIds, $orderLists); //list unique orders
            $orderListings = OrdersController::orderList($uniqueOrders); //orderListing with total amount
            if (!empty($orderListings) && is_array($orderListings)) {
                foreach ($orderListings as $orderListing) {
                    $data['orderList'] = $orderListing;
                }
            } else {
                $data['orderList'] = array();
            }
        } else {
            $data['orderList'] = array();
        }

        $data['dataList'] = new CArrayDataProvider($data['orderList'], array('pagination' => array('pageSize' => 10)));

        $data['sortingClass']['orderId'] = 'sorting';
        $data['sortingClass']['date'] = 'sorting';
        $data['sortingClass']['name'] = 'sorting';
        $data['sortingClass']['status'] = 'sorting';
        $data['sortingClass']['amount'] = 'sorting';
        if (isset($sortBy)) {
            if ($sortBy == 'id') {
                $data['sortingClass']['orderId'] = $orderClass;
            }
            if ($sortBy == 'createdDate') {
                $data['sortingClass']['date'] = $orderClass;
            }
            if ($sortBy == 'fullName') {
                $data['sortingClass']['name'] = $orderClass;
            }
            if ($sortBy == 'orderStatus') {
                $data['sortingClass']['status'] = $orderClass;
            }
            if ($sortBy == 'productPrice') {
                $data['sortingClass']['amount'] = $orderClass;
            }
        }

        $this->render('orderList', $data);
    }


    /**
     *
     * @param array $orders
     * @return type array
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionGetOrder($orders)
    {
        foreach ($orders as $order) {
            foreach ($order as $orderId => $amount) {
                if (isset($results[$orderId])) {
                    $results[$orderId] += $amount;
                } else {
                    $results[$orderId] = $amount;
                }
            }
        }
        return $results;
    }

    /**
     *
     * @param array $results
     * @return type array
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionListingOrders($results)
    {
        foreach ($results as $key => $result) {
            $orderId = (int) str_replace("order", "", $key);
            $order = Orders::model()->findByPk($orderId);
            $orderAttributes = $order->attributes;
            $orderAttributes['productPrice'] = $result;
            $orderAttributes['discount'] = OrderVendor::getDiscountByOrderId($orderId, $orderAttributes['productPrice']);
            $orderAttributes['shipping'] = OrderDetails::getShippingCost($orderId);
            $orderAttributes['totalPrice'] = $this->actionTotalOrderPrice($orderAttributes['productPrice'], $orderAttributes['taxPercent'], $orderAttributes['discount'], $orderAttributes['shipping']);
            $user = Users::customerInformation($orderAttributes['customerId']);
            $orderAttributes['fullName'] = $user['fullname'];
            $orderAttributes['orderStatus'] = OrderVendor::getOrderStatus($orderId);
            $orderListings[] = $orderAttributes;
        }
        return $orderListings;
    }

    /**
     * get total calculated order price
     * @param float $price
     * @param float $tax
     * @param float $discount
     * @param float $shipping
     * @return type float
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionTotalOrderPrice($price, $tax, $discount, $shipping)
    {
        $discountedPrice = $price - $discount;
        $taxAdded = ($tax / 100) * $discountedPrice;
        $total = $discountedPrice + $taxAdded + $shipping;
        return $total;
    }

    /**
     * export order list.
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionExportOrder()
    {
        if (isset($_GET['key']) && !empty($_GET['key'])) {
            $searchKey = mysql_escape_string($_GET['key']);
            if (isset($searchKey) && !empty($searchKey)) {
                $query = Orders::getSearchQuery($searchKey); //sql raw query with search parameter
            }
        } else {
            $query = Orders::getQuery(); //sql raw query
        }
        //get order status
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        } else {
            $status = 'all';
        }
        //append on sql query based on status i.e all, pending, completed, cancelled
        $query.= Orders::getOrderStatusSql($status);

        if (!empty($_GET['checkedValues'])) {
            $checkedArrays = $_GET['checkedValues'];
            if ($checkedArrays[0] == 'on') {
                unset($checkedArrays[0]);
                $str = implode(',', $checkedArrays);
            } else {
                $str = implode(',', $checkedArrays);
            }
            $query.= " AND o.id in($str)";
        }

        $query.= " ORDER BY o.id DESC";


        $orderLists = Common::getSqlResult($query); //lists order
        if (is_array($orderLists) && !empty($orderLists)) {
            $uniqueOrderIds = OrdersController::getUniqueOrderId($orderLists); //list unique orderId
            $uniqueOrders = OrdersController::uniqueOrder($uniqueOrderIds, $orderLists); //list unique orders
            $orderListings = OrdersController::orderList($uniqueOrders); //orderListing with total amount
            if (!empty($orderListings) && is_array($orderListings)) {
                foreach ($orderListings as $orderListing) {
                    $data['exportOrderList'] = $orderListing;
                }
            } else {
                $data['exportOrderList'] = array();
            }
        } else {
            $data['exportOrderList'] = array();
        }

        if (isset($_GET['export_csv'])) {
            $export_data = $_GET['export_csv'];
        } else {
            $export_data = 0;
        }

        if ($export_data == 1) {
            if ($data['exportOrderList'] && is_array($data['exportOrderList'])) {
                foreach ($data['exportOrderList'] as $row) {
                    $exportList[] = array(
                        'ORDER' => CHtml::encode($row['id']),
                        'DATE' => Yii::app()->dateFormatter->format("MMMM dd, yyyy", CHtml::encode($row['createdDate'])),
                        'NAME' => CHtml::encode($row['fullName']),
                        'ORDER STATUS' => CHtml::encode($row['status']),
                        'AMOUNT' => "$" . CHtml::encode($row['totalPrice'])
                    );
                }
            } else {
                $exportList[] = array(
                    'ORDER' => '',
                    'DATE' => '',
                    'NAME' => '',
                    'ORDER STATUS' => '',
                    'AMOUNT' => ''
                );
            }
            $csv = new ECSVExport($exportList);
            $content = $csv->toCSV();
            Yii::app()->getRequest()->sendFile('order ' . Yii::app()->dateFormatter->format("MMMM dd, yyyy", date('Y-m-d')) . '.csv', $content, "text/csv", false);
            exit();
        }
    }

    /**
     * change order status on selecting the status.
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionChangeStatus()
    {
        $changeStatus = $_GET['changeStatus'];
        $checkedValues = $_GET['checkedValues'];
        $orderVendor = new OrderVendor();
        if (!empty($changeStatus) && !empty($checkedValues)) {
            if ($checkedValues[0] == 'on') {
                unset($checkedValues[0]);
            }
            foreach ($checkedValues as $k => $v) {
                $vendorStatus = $orderVendor->changeStatus($v, $changeStatus);
            }
            if ($vendorStatus) {
                $array = array('status' => 'success', 'msg' => 'Order status changed successfully.');
            } else {
                $array = array('status' => 'error', 'msg' => 'Failed to change status.');
            }
            echo json_encode($array);
            exit;
        }
    }

    /**
     * lists order detail
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionOrderDetail()
    {
        if (!isset(Yii::app()->user->userType)) {
            $this->redirect(Yii::app()->request->baseUrl . '/login');
        } else {
            $newState = Yii::app()->user->userType;
            Yii::app()->user->setState('userType', 'Vendor');
            $actualState = Yii::app()->user->getState('userType');
            if ($newState !== $actualState) {
                $this->redirect(Yii::app()->request->baseUrl . '/login');
            }
        }

        $orderId = (int) Yii::app()->request->getParam('id');
        if (!empty($orderId)) {
            $query = Orders::getOrderDetailQuery($orderId);
            $orderDetails = Common::getSqlResult($query);
            if (!empty($orderDetails) && is_array($orderDetails)) {
                $orderVendor = array();
                if (!empty($orderDetails) && is_array($orderDetails)) {
                    foreach ($orderDetails as $orderDetail) {
                        $vendorInfo = Users::customerInformation($orderDetail['customerId']); //vendor detail information
                        $orderDetail['personalInfo'] = $vendorInfo;
                        $orderDetail['address'] = $this->actionVendorShippingAddress($orderDetail['shippingAddressId']); //vendro shipping detail information
                        $orderDetail['amount'] = OrderVendor::getOrderAmount($orderDetail['productPrice'], $orderDetail['quantity'], $orderDetail['couponDiscount'], $orderDetail['couponDiscountType'], $orderDetail['taxPercent'], $orderDetail['shippingPrice']);
                        $orderDetail['productOption'] = Productoption::getProductOptionDetail($orderDetail['productOptionId']);
                        $orderVendor[] = $orderDetail;
                    }
                }
                $item = 0;
                $discount = 0;
                $tax = 0;
                $shipping = 0;
                $total = 0;
                if (!empty($orderVendor) && is_array($orderVendor)) {
                    foreach ($orderVendor as $orderVendorInfo) {
                        $item+= $orderVendorInfo['amount']['productPrice'];
                        $discount+= $orderVendorInfo['amount']['discount'];
                        $tax+= $orderVendorInfo['amount']['tax'];
                        $shipping+= $orderVendorInfo['amount']['shipping'];
                    }
                }
                $totalPrice = ($item + -$discount) + $tax + $shipping;
                $orderAmount = array('item' => $item, 'discount' => $discount, 'tax' => $tax, 'shipping' => $shipping, 'totalPrice' => $totalPrice);
                if (!empty($orderVendor[0]['orderId'])) {
                    $order = $orderVendor[0]['orderId'];
                } else {
                    $order = $orderId;
                }
                if (!empty($orderVendor[0]['personalInfo'])) {
                    $info = $orderVendor[0]['personalInfo'];
                } else {
                    $info = '';
                }
                if (!empty($orderVendor[0]['address'])) {
                    $addr = $orderVendor[0]['address'];
                } else {
                    $addr = '';
                }
                if (!empty($orderVendor[0]['createdDate'])) {
                    $date = $orderVendor[0]['createdDate'];
                } else {
                    $date = '';
                }
            } else {
                throw new CHttpException(400, 'Bad Request');
            }
        } else {
            throw new CHttpException(400, 'Bad Request');
        }
        $this->render('orderDetail', array(
            'orderId' => $order,
            'personalInfo' => $info,
            'address' => $addr,
            'orderDetails' => $orderVendor,
            'orderAmount' => $orderAmount,
            'orderHistory' => $date,
        ));
    }

    /**
     * get total price of product items
     * @param array $priceDetails
     * @return float
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function getOrderPriceItem($priceDetails)
    {
        foreach ($priceDetails as $priceDetail) {
            $orderVendorId = $priceDetail['orderVendorId'];
            $price['items'] = OrderDetails::getOrderPrice($orderVendorId);
            $orderPrices[] = $price;
        }
        $itemsPrice = 0;
        foreach ($orderPrices as $orderPrice) {
            $itemsPrice+= $orderPrice['items'];
        }
        return $itemsPrice;
    }

    /**
     * get total shipping price of product items
     * @param array $priceDetails
     * @return float
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function getTotalShippingCharge($orderDetails)
    {
        foreach ($orderDetails as $orderDetail) {
            $orderVendorId = $orderDetail['orderVendorId'];
            $price['shipping'] = OrderDetails::getShippingCharge($orderVendorId);
            $orderPrices[] = $price;
        }
        $itemsPrice = 0;
        foreach ($orderPrices as $orderPrice) {
            $itemsPrice+= $orderPrice['shipping'];
        }
        return $itemsPrice;
    }

    /**
     *
     * @param int $shippingId
     * @return type array
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionVendorShippingAddress($shippingId)
    {
        $addresses = CustomerAddress::model()->findByPk($shippingId);
        $address['customerName'] = $addresses['fullName'];
        $address['addressLabel'] = $addresses['addressLabel'];
        $address['addressLine1'] = $addresses['addressLine1'];
        $address['addressLine2'] = $addresses['addressLine2'];
        $address['city'] = $addresses['city'];
        $address['postalCode'] = $addresses['postalCode'];
        $address['stateId'] = $addresses['stateId'];
        $state = State::model()->findByPk($address['stateId']);
        $address['state'] = $state['code'];
        $address['countryId'] = $addresses['countryId'];
        $country = Country::model()->findByPk($address['countryId']);
        $address['country'] = $country['name'];
        return $address;
    }

    /**
     * change the order status
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionChangeProductStatus()
    {
        $changeStatus = Yii::app()->request->getParam('changeStatus');
        $checkedValues = Yii::app()->request->getParam('checkedValues');

        $checkEmailStatus = Notification::model()->findByAttributes(array(
            'vendorId' => Yii::app()->user->id
        ));

        if (!empty($changeStatus) && !empty($checkedValues)) {
            foreach ($checkedValues as $k => $v) {
                $productStatus = OrderVendor::changeStatusById($v, $changeStatus);

                if ($v != 'on') {
                    NewsActivity::OrderProductStatus($v);

                    if (1 === (int) $checkEmailStatus->cancelOrder && "Cancelled" == $changeStatus) {
                        Order::sendCancelOrderEmailToCustomer($v);
                    }
                }
            }

            if ($productStatus) {
                $array = array('status' => 'success', 'currentStatus' => $changeStatus, 'msg' => 'Status changed successfully.');
            } else {
                $array = array('status' => 'error', 'msg' => 'Failed to change status.');
            }
            echo json_encode($array);
            exit;
        }
    }

    /*     * *****ORDERS LISTS ON ADMIN SIDE******************* */

    public function actionAdminOrderList()
    {
        if (!isset(Yii::app()->user->userType)) {
            $this->redirect(Yii::app()->request->baseUrl . '/login');
        } else {
            $newState = Yii::app()->user->userType;
            Yii::app()->user->setState('userType', 'Admin');
            $actualState = Yii::app()->user->getState('userType');
            if ($newState !== $actualState) {

                $this->redirect(Yii::app()->request->baseUrl . '/login');
            }
        }

        $this->layout = '//layouts/admin'; //admin layout

        $criteria = new CDbCriteria();
        $criteria->order = 'id DESC';
        $orderLists = Orders::model()->findAll($criteria);
        if (!empty($orderLists) && is_array($orderLists)) {
            foreach ($orderLists as $orderList) {
                $orderAttributes = $orderList->attributes;
                $user = Users::getOrderCustomerName($orderList->customerId);
                $orderAttributes['amount'] = OrderVendor::getAdminOrderVendors($orderAttributes['id'], $orderAttributes['taxPercent']);
                $orderAttributes['orderPaid'] = OrderVendor::getOrderPaidStatus($orderAttributes['id']);
                $orderAttributes['fullName'] = $user['fullname'];
                $orders['orderAttributes'][] = $orderAttributes;
            }

            //search order listing
            if (!empty($_GET['key'])) {
                $searchKey = mysql_escape_string($_GET['key']);
                $searchedOrder = array();
                foreach ($orders['orderAttributes'] as $searchOrderInfo) {
                    if (is_numeric($searchKey)) {
                        $searchType = "numeric";
                    } else {
                        $searchType = "alphaNumeric";
                    }
                    if ($this->actionSearchOrder($searchOrderInfo, $searchKey, $searchType)) {
                        $searchedOrder[] = $searchOrderInfo;
                    }
                }
                $orders['orderAttributes'] = $searchedOrder;
            }

            //sort order list
            if (!empty($_GET['sort'])) {
                $orderBy = $_GET['sort'];
                $sortBy = $_GET['sortKey'];
                if ($orderBy == 'asc') {
                    $orderClass = 'sorting-up';
                } else {
                    $orderClass = 'sorting-down';
                }
                $orders['orderAttributes'] = $this->actionOrderSort($orders['orderAttributes'], $sortBy, $orderBy);
            }

            //export selected order list
            if (!empty($_GET['checkedValues'])) {
                $checkedArrays = $_GET['checkedValues'];
                if ($checkedArrays[0] == 'on') {
                    $checkedArrays = $_GET['checkedValues'];
                    array_shift($checkedArrays);
                }
                $exportList = array();
                foreach ($checkedArrays as $checkedArray) {
                    if (!empty($orders['orderAttributes']) && is_array($orders['orderAttributes'])) {
                        $temp = array();
                        foreach ($orders['orderAttributes'] as $ordersAttr) {
                            if ($ordersAttr['id'] == $checkedArray) {
                                $temp = $ordersAttr;
                            }
                        }
                        $exportList[] = $temp;
                    }
                }
                $exportCustomerList = $this->actionExportAdminOrder($exportList);
            } else {
                $exportOrderList = $this->actionExportAdminOrder($orders['orderAttributes']);
            }
        }

        if (!empty($orders) && is_array($orders)) {
            foreach ($orders as $order) {
                $data['orderList'] = $order;
            }
        } else {
            $data['orderList'] = array();
        }
        $dataOrder = array();
        $dataOrder['dataList'] = new CArrayDataProvider($data['orderList'], array('pagination' => array('pageSize' => 10)));
        $dataOrder['sortingClass']['id'] = 'sorting';
        $dataOrder['sortingClass']['createdDate'] = 'sorting';
        $dataOrder['sortingClass']['fullName'] = 'sorting';
        $dataOrder['sortingClass']['status'] = 'sorting';
        $dataOrder['sortingClass']['amount'] = 'sorting';
        if (isset($sortBy)) {
            if ($sortBy == 'id') {
                $dataOrder['sortingClass']['id'] = $orderClass;
            }
            if ($sortBy == 'createdDate') {
                $dataOrder['sortingClass']['createdDate'] = $orderClass;
            }
            if ($sortBy == 'fullName') {
                $dataOrder['sortingClass']['fullName'] = $orderClass;
            }
            if ($sortBy == 'status') {
                $dataOrder['sortingClass']['status'] = $orderClass;
            }
            if ($sortBy == 'amount') {
                $dataOrder['sortingClass']['amount'] = $orderClass;
            }
        }
        $this->render('adminOrderList', $dataOrder);
    }

    /**
     * sort order lists.
     * @param string $orderInfo
     * @param string $sortBy
     * @param array $orderBy
     * @return array $orderInfo
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionOrderSort($orderInfo, $sortBy, $orderBy)
    {
        if ($sortBy == 'id') {
            if ($orderBy == 'asc') {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['id'] == $value2['id']) {
                        return 0;
                    } else if ($value1['id'] > $value2['id']) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
            } else {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['id'] == $value2['id']) {
                        return 0;
                    } else if ($value1['id'] > $value2['id']) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
            }
        } else if ($sortBy == 'createdDate') {
            if ($orderBy == 'asc') {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['createdDate'] == $value2['createdDate']) {
                        return 0;
                    } else if ($value1['createdDate'] > $value2['createdDate']) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
            } else {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['createdDate'] == $value2['createdDate']) {
                        return 0;
                    } else if ($value1['createdDate'] > $value2['createdDate']) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
            }
        } else if ($sortBy == 'fullName') {
            if ($orderBy == 'asc') {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['fullName'] == $value2['fullName']) {
                        return 0;
                    } else if ($value1['fullName'] > $value2['fullName']) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
            } else {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['fullName'] == $value2['fullName']) {
                        return 0;
                    } else if ($value1['fullName'] > $value2['fullName']) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
            }
        } else if ($sortBy == 'amount') {
            if ($orderBy == 'asc') {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['amount'] == $value2['amount']) {
                        return 0;
                    } else if ($value1['amount'] > $value2['amount']) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
            } else {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['amount'] == $value2['amount']) {
                        return 0;
                    } else if ($value1['amount'] > $value2['amount']) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
            }
        } else if ($sortBy == 'status') {
            if ($orderBy == 'asc') {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['orderPaid'] == $value2['orderPaid']) {
                        return 0;
                    } else if ($value1['orderPaid'] > $value2['orderPaid']) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
            } else {
                usort($orderInfo, function($value1, $value2) {
                    if ($value1['orderPaid'] == $value2['orderPaid']) {
                        return 0;
                    } else if ($value1['orderPaid'] > $value2['orderPaid']) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
            }
        }
        return $orderInfo;
    }

    /**
     * searches on orderId, name, amount.
     * @param array $searchOrderInfo
     * @param number/alphanumeric $searchKey
     * @param string $searchType
     * @return boolean
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public static function actionSearchOrder($searchOrderInfo, $searchKey, $searchType = "alphaNumeric")
    {
        if ($searchType == "alphaNumeric") {
            $searchArrayKey = array('fullName');
            foreach ($searchOrderInfo as $key => $value) {
                if (in_array($key, $searchArrayKey)) {
                    $pattern = '/' . $searchKey . '/i';
                    if (preg_match($pattern, $value) > 0) {
                        return TRUE;
                    }
                }
            }
            return FALSE;
        } else {
            $searchArrayKey = array('id', 'amount');
            foreach ($searchOrderInfo as $key => $value) {
                if (in_array($key, $searchArrayKey)) {
                    $pattern = '/^' . $searchKey . '$/';
                    if (preg_match($pattern, $value) > 0) {
                        return TRUE;
                    }
                }
            }
            return FALSE;
        }
    }

    /**
     * exports order list.
     * @param array $orderInfo
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionExportAdminOrder($orderInfo)
    {
        if (!empty($orderInfo) && is_array($orderInfo)) {
            foreach ($orderInfo as $orders) {
                $data['exportOrderList'][] = $orders;
            }
        } else {
            $data['exportOrderList'] = array();
        }

        if (isset($_GET['export_csv'])) {
            $export_data = $_GET['export_csv'];
        } else {
            $export_data = 0;
        }
        if ($export_data == 1) {
            if ($data['exportOrderList'] && is_array($data['exportOrderList'])) {
                foreach ($data['exportOrderList'] as $row) {
                    if (!empty($row['amount'])) {
                        $amount = "$" . $row['amount'];
                    } else {
                        $amount = '';
                    }
                    $exportList[] = array(
                        'ORDERID' => CHtml::encode($row['id']),
                        'DATE' => Yii::app()->dateFormatter->format("MMMM dd, yyyy", CHtml::encode($row['createdDate'])),
                        'CUSTOMER NAME' => CHtml::encode($row['fullName']),
                        'STATUS' => CHtml::encode($row['orderPaid']),
                        'AMOUNT' => CHtml::encode($amount)
                    );
                }
            } else {
                $exportList[] = array(
                    'ORDERID' => '',
                    'DATE' => '',
                    'CUSTOMER NAME' => '',
                    'STATUS' => '',
                    'AMOUNT' => ''
                );
            }
            $csv = new ECSVExport($exportList);
            $content = $csv->toCSV();
            Yii::app()->getRequest()->sendFile('order ' . Yii::app()->dateFormatter->format("MMMM dd, yyyy", date('Y-m-d')) . '.csv', $content, "text/csv", false);
            exit();
        }
    }

    /**
     * list vendor order details
     * @throws CHttpException 404
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionAdminOrderDetails()
    {
        if (!isset(Yii::app()->user->userType)) {
            $this->redirect(Yii::app()->request->baseUrl . '/login');
        } else {
            $newState = Yii::app()->user->userType;
            Yii::app()->user->setState('userType', 'Admin');
            $actualState = Yii::app()->user->getState('userType');
            if ($newState !== $actualState) {

                $this->redirect(Yii::app()->request->baseUrl . '/login');
            }
        }

        $this->layout = '//layouts/admin'; //admin layout

        if (isset($_GET['id'])) {
            $orderId = Yii::app()->request->getParam('id');
            $orderList = Orders::model()->findByPk($orderId);
            if (!empty($orderList)) {
                $orderAttributes = $orderList->attributes;
                $user = Users::getOrderCustomerName($orderAttributes['customerId']);
                $orderAttributes['amount'] = OrderVendor::getAdminOrderVendors($orderAttributes['id'], $orderAttributes['taxPercent']); //total amount
                $orderAttributes['orderPaid'] = OrderVendor::getOrderPaidStatus($orderList->id);
                $orderAttributes['fullName'] = $user['fullname'];
                $vendors = OrderVendor::getvendorInformation($orderAttributes['id'], $orderAttributes['taxPercent']); //vendor personal and bank detail information
                $orderAttributes['vendors'] = $vendors;
                $orders['orderAttributes'] = $orderAttributes;
            } else {
                throw new CHttpException(400, 'Bad Request!!');
            }
        } else {
            throw new CHttpException(404, 'Request not found.');
        }
        $this->render('adminOrderDetail', $orders);
    }

    /**
     * changes the paid status of ordered vendor.
     * @author Sundar Ban <sundarban@lftechnology.com>
     */
    public function actionChangeAdminOrderVendorStatus()
    {
        $changeStatus = Yii::app()->request->getParam('changeStatus');
        $checkedValues = Yii::app()->request->getParam('checkedValues');
        $orderId = Yii::app()->request->getParam('orderId');
        if (!empty($changeStatus) && !empty($checkedValues)) {
            foreach ($checkedValues as $k => $v) {
                $orderStatus = OrderVendor::model()->changeOrderPaidStatus($v, $orderId, $changeStatus);
            }
            if ($orderStatus) {
                $array = array('status' => 'success', 'currentStatus' => $changeStatus, 'msg' => 'Status changed successfully.');
            } else {
                $array = array('status' => 'error', 'msg' => 'Failed to change status.');
            }
            echo json_encode($array);
            exit;
        }
    }

}